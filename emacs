(let ((default-directory "~/dotfiles/elisp/"))
      (normal-top-level-add-subdirs-to-load-path))

;(autoload 'python-mode "python-mode.el" "Python mode." t)
(setq auto-mode-alist (append '(("/*.\.py$" . python-mode)) auto-mode-alist))




;;;; Global Keybindings

(global-set-key [f1] 'compile)
(global-set-key [f2] 'magit-status)
(global-set-key "\M-g" 'goto-line)

;; reload config
(global-set-key [f5] '(lambda () (interactive) (load-file "~/.emacs")))

;; show line numbers
(autoload 'linum-mode "linum" "toggle line numbers on/off" t)
(global-set-key (kbd "C-<f5>") 'linum-mode)

;;; Use "C-5" to jump to the matching parenthesis.
(defun goto-match-paren (arg)
  "Go to the matching parenthesis if on parenthesis, otherwise insert
the character typed."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
    ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
    (t                    (nil)) ))

(global-set-key (kbd "C-5") 'goto-match-paren)


;; Insert new empty line (vi style)
(define-key global-map [(meta return)] 'vi-open-line)


;; fully global keybindings
(defvar drew-minor-mode-map (make-keymap) "drew-minor-mode keymap.")
(define-key drew-minor-mode-map (kbd "M-h") 'backward-char)
(define-key drew-minor-mode-map (kbd "M-l") 'forward-char)
(define-key drew-minor-mode-map (kbd "M-j") 'next-line)
(define-key drew-minor-mode-map (kbd "M-k") 'previous-line)
;;(define-key drew-minor-mode-map (kbd "C-c C-c") 'comment-region)
(define-minor-mode drew-minor-mode
  "A minor mode so that my key settings aren't shadowed by other major/minor modes"
  t " drew" 'drew-minor-mode-map)


;; align-regexp
(global-set-key (kbd "C-x a r") 'align-regexp)

;; Add F12 to toggle line wrap
(global-set-key [f12] 'toggle-truncate-lines)

(global-set-key (kbd "M-<up>") (lambda () (interactive) (previous-line 5)))

;; override default set fill-column to find-file (C-x f -> C-x C-f
(global-set-key (kbd "C-x f") 'find-file)

;;;; Misc Config

;; no icon toolbar
(tool-bar-mode -1)

;; Text and the such
;; Use colors to highlight commands, etc.
(global-font-lock-mode t)

;; Disable the welcome message
(setq inhibit-startup-message t)

;; Format the title-bar to always include the buffer name
(setq frame-title-format "emacs - %b")

;; Make the mouse wheel scroll Emacs
(mouse-wheel-mode t)

;; Flash instead of that annoying bell
(setq visible-bell t)

;; Use y or n instead of yes or not
(fset 'yes-or-no-p 'y-or-n-p)


;; normal scrollbar on right side
(setq scroll-bar-mode-explicit t)
    (set-scroll-bar-mode `right)

;; highlight matchin paren
(show-paren-mode t)

;; stop leaving backup~ turds scattered everywhere
(setq backup-directory-alist '(("." . "~/.emacs-backups")))
(setq make-backup-files nil)
(setq auto-save-default nil)


;; use spaces instead of tabs
(setq-default indent-tabs-mode nil)


;; scroll as compile output grows
(setq compilation-scroll-output t)


;; wrap lines in some modes
(add-hook 'markdown-mode-hook 'turn-on-visual-line-mode)
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)


;; Don't alarm bell when scrolling to end of document
;; http://stackoverflow.com/questions/324457/disable-carbon-emacs-scroll-beep
(defun my-bell-function ()
  (unless (memq this-command
        '(isearch-abort abort-recursive-edit exit-minibuffer
              keyboard-quit mwheel-scroll down up next-line previous-line
              backward-char forward-char))
    (ding)))
(setq ring-bell-function 'my-bell-function)



;; better search
(add-hook 'isearch-mode-hook
 (lambda ()
 (define-key isearch-mode-map (kbd "M-s") 'isearch-repeat-forward)
 (define-key isearch-mode-map (kbd "M-S") 'isearch-repeat-backward)
 )
)


;; Allow mouse to change focus, like xmonad does between windows
(setq focus-follows-mouse t)
(setq mouse-autoselect-window t)


;; See http://www.delorie.com/gnu/docs/elisp-manual-21/elisp_620.html
;; and http://www.gnu.org/software/emacs/manual/elisp.pdf

;; disable line wrap
(setq default-truncate-lines t)

;; make side by side buffers function the same as the main window
(setq truncate-partial-width-windows nil)



;; Overwrite flymake-display-warning so that no annoying dialog box is
;; used.
;; TODO doesn't work

;; This version uses lwarn instead of message-box in the original version.
;; lwarn will open another window, and display the warning in there.
(defun flymake-display-warning (warning)
  "Display a warning to the user, using lwarn"
  (lwarn 'flymake :warning warning))

;; Using lwarn might be kind of annoying on its own, popping up windows and
;; what not. If you prefer to recieve the warnings in the mini-buffer, use:
(defun flymake-display-warning (warning)
  "Display a warning to the user, using lwarn"
  (message warning))



;; ignored tab complete extensions

;; todo consider using icicles
(setq completion-ignored-extensions
      '(".hi" ".o" ".pyc"))

(defadvice completion--file-name-table (after
                                        ignoring-backups-f-n-completion
                                        activate)
  "filter out results when the have completion-ignored-extensions"
  (let ((res ad-return-value))
(if (and (listp res)
     (stringp (car res))
     (cdr res))                 ; length > 1, don't ignore sole match
    (setq ad-return-value
              (completion-pcm--filename-try-filter res)))))





;;;; Default major modes

(setq auto-mode-alist (append
     '(("\\.cu$" . c++-mode))
       auto-mode-alist))

(setq auto-mode-alist (append
     '(("\\zshrc$" . shell-script-mode))
       auto-mode-alist))

(setq auto-mode-alist (append
     '(("\\emacs$" . emacs-lisp-mode))
       auto-mode-alist))

(setq auto-mode-alist (append
     '(("\\rc$" . shell-script-mode))
       auto-mode-alist))

(setq auto-mode-alist (cons '("\\.lua$" . lua-mode) auto-mode-alist))
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)

(add-hook 'lua-mode-hook 'turn-on-font-lock)


(setq auto-mode-alist (append
     '(("\\rake$" . ruby-mode))
       auto-mode-alist))

(require 'protobuf-mode)
(setq auto-mode-alist (append
     '(("\\.proto$" . protobuf-mode))
       auto-mode-alist))

;;;; Magit

(autoload 'magit-status "magit" nil t)
;; change magit diff colors
 (eval-after-load 'magit
   '(progn
      (set-face-foreground 'magit-diff-add "green3")
      (set-face-foreground 'magit-diff-del "red2")
      (set-face-foreground 'magit-diff-hunk-header "yellow")
      (set-face-foreground 'magit-section-title "lightblue")
      (set-face-foreground 'magit-header "orange")
      (set-face-foreground 'magit-diff-file-header "orange")
      (set-face-background 'magit-diff-none "#000000")
      (set-face-background 'magit-item-highlight "#111122")))

;;;; Markdown mode

(autoload 'markdown-mode "markdown-mode.el"
   "Major mode for editing Markdown files" t)
(setq auto-mode-alist
   (cons '("\.md" . markdown-mode) auto-mode-alist))



;;;; Haskell

;; haskell mode configuration
(setq auto-mode-alist
      (append auto-mode-alist
              '(("\\.[hg]s$"  . haskell-mode)
                ("\\.hic?$"     . haskell-mode)
                ("\\.hsc$"     . haskell-mode)
  ("\\.chs$"    . haskell-mode)
                ("\\.l[hg]s$" . literate-haskell-mode))))
(autoload 'haskell-mode "haskell-mode"
   "Major mode for editing Haskell scripts." t)
(autoload 'literate-haskell-mode "haskell-mode"
   "Major mode for editing literate Haskell scripts." t)

;adding the following lines according to which modules you want to use:
(require 'inf-haskell)

(add-hook 'haskell-mode-hook 'turn-on-font-lock)
;(add-hook 'haskell-mode-hook 'turn-off-haskell-decl-scan)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)
(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)
;(add-hook 'haskell-mode-hook 'turn-on-haskell-hugs)



;;(remove-hook 'haskell-mode-hook 'turn-on-haskell-indent)


(require 'flymake)

(defun flymake-Haskell-init ()
  (flymake-simple-make-init-impl
   'flymake-create-temp-with-folder-structure nil nil
   (file-name-nondirectory buffer-file-name)
   'flymake-get-Haskell-cmdline))

(defun flymake-get-Haskell-cmdline (source base-dir)
  (list "/home/drew/emacs/flycheck_haskell.pl"
        (list source base-dir)))

(push '(".+\\.hs$" flymake-Haskell-init flymake-simple-java-cleanup)
      flymake-allowed-file-name-masks)
(push '(".+\\.lhs$" flymake-Haskell-init flymake-simple-java-cleanup)
      flymake-allowed-file-name-masks)
(push
 '("^\\(\.+\.hs\\|\.lhs\\):\\([0-9]+\\):\\([0-9]+\\):\\(.+\\)"
   1 2 3 4) flymake-err-line-patterns)

;; optional setting
;; if you want to use flymake always, then add the following hook.
(add-hook
 'haskell-mode-hook
 '(lambda ()
    (if (not (null buffer-file-name)) (flymake-mode))))

(when (fboundp 'resize-minibuffer-mode) ; for old emacs
  (resize-minibuffer-mode)
  (setq resize-minibuffer-window-exactly nil))

(add-hook
 'haskell-mode-hook
 '(lambda ()
    (define-key haskell-mode-map "\C-cd"
      'credmp/flymake-display-err-minibuf)))

(defun credmp/flymake-display-err-minibuf ()
  "Displays the error/warning for the current line in the minibuffer"
  (interactive)
  (let* ((line-no             (flymake-current-line-no))
         (line-err-info-list  (nth 0 (flymake-find-err-info flymake-err-info line-no)))
         (count               (length line-err-info-list))
         )
    (while (> count 0)
      (when line-err-info-list
        (let* ((file       (flymake-ler-file (nth (1- count) line-err-info-list)))
               (full-file  (flymake-ler-full-file (nth (1- count) line-err-info-list)))
               (text (flymake-ler-text (nth (1- count) line-err-info-list)))
               (line       (flymake-ler-line (nth (1- count) line-err-info-list))))
          (message "[%s] %s" line text)
          )
        )
      (setq count (1- count)))))


(defun hask-compile ()
 (inferior-haskell-load-and-run)
(interactive)
                                                (end-of-buffer)
                                                )

(add-hook 'haskell-mode-hook
          '(lambda ()
             (define-key haskell-mode-map [f1] 'inferior-haskell-load-and-run
)))



;; WHITESPACE

(require 'ethan-wspace)
(global-ethan-wspace-mode 1)


(require 'nginx-mode)



;; To use manually: M-x doc-mode
;; a mode designed for editing text file documents that contain a lot
;; of text to spell check, etc.
(defun doc-mode ()
(interactive)
(text-mode) ;; use regular text mode
(auto-fill-mode) ;; wrap lines
;; (refill-mode) ;; wrap lines (more aggressive)
(flyspell-mode)) ;; underline misspelled words
;; run M-x flyspell-buffer for it to underline all
;; words in the file instead of the words your
;; cursor moves over.


    (defun unwrap-line ()
      "Remove all newlines until we get to two consecutive ones.
    Or until we reach the end of the buffer.
    Great for unwrapping quotes before sending them on IRC."
      (interactive)
      (let ((start (point))
            (end (copy-marker (or (search-forward "\n\n" nil t)
                                  (point-max))))
            (fill-column (point-max)))
        (fill-region start end)
        (goto-char end)
        (newline)
        (goto-char start)))
(put 'downcase-region 'disabled nil)


(setq auto-mode-alist (cons '("\\.t$" . perl-mode) auto-mode-alist))

;; case insensitive minibuffer tab completion
(setq read-file-name-completion-ignore-case t)

(autoload 'python-mode "python.el" "Python mode." t)
(setq auto-mode-alist (append '(("/*.\.py$" . python-mode)) auto-mode-alist))



(autoload 'pymacs-apply "pymacs")
(autoload 'pymacs-call "pymacs")
(autoload 'pymacs-eval "pymacs" nil t)
(autoload 'pymacs-exec "pymacs" nil t)
(autoload 'pymacs-load "pymacs" nil t)
;;(eval-after-load "pymacs"
;;  '(add-to-list 'pymacs-load-path YOUR-PYMACS-DIRECTORY"))


(when (load "flymake" t)
  (defun flymake-pyflakes-init ()
     ; Make sure it's not a remote buffer or flymake would not work
      (let* ((temp-file (flymake-init-create-temp-buffer-copy
                         'flymake-create-temp-inplace))
             (local-file (file-relative-name
                          temp-file
                          (file-name-directory buffer-file-name))))
        (list "pyflakes" (list local-file))))
  (add-to-list 'flymake-allowed-file-name-masks
               '("\\.py\\'" flymake-pyflakes-init)))

(add-hook 'python-mode-hook
      (lambda ()
        (unless (eq buffer-file-name nil) (flymake-mode 1)) ;dont invoke flymake on temporary buffers for the interpreter
        (local-set-key [f2] 'flymake-goto-prev-error)
        (local-set-key [f3] 'flymake-goto-next-error)
        ))



(require 'uniquify)


;; M-backspace works when opening file in minibuffer
(define-key minibuffer-local-map [M-backspace] 'ido-delete-backward-word-updir)




;; buffer switching
(ido-mode 1)




(defun vi-open-line-above ()
  "Insert a newline above the current line and put point at beginning."
  (interactive)
  (unless (bolp)
    (beginning-of-line))
  (newline)
  (forward-line -1)
  (indent-according-to-mode))

(defun vi-open-line-below ()
  "Insert a newline below the current line and put point at beginning."
  (interactive)
  (unless (eolp)
    (end-of-line))
  (newline-and-indent))

(defun vi-open-line (&optional abovep)
  "Insert a newline below the current line and put point at beginning.
With a prefix argument, insert a newline above the current line."
  (interactive "P")
  (if abovep
      (vi-open-line-above)
    (vi-open-line-below)))



(setq default-tab-width 4)
(put 'upcase-region 'disabled nil)



(setq-default truncate-lines t)
(setq-default truncate-partial-width-windows nil)

; will make "Ctrl-k" kills an entire line if the cursor is at the beginning of line -- very useful.
(setq kill-whole-line t)



(defun switch-to-minibuffer-window ()
  "switch to minibuffer window (if active)"
  (interactive)
  (when (active-minibuffer-window)
    (select-window (active-minibuffer-window))))
(define-key global-map "\M-=" 'switch-to-minibuffer-window)

(global-set-key (kbd "M-9") 'kill-whole-line)


(global-set-key [M-delete] 'kill-word)



; mouse-2 will paste at point rather than at click
(setq mouse-yank-at-point t)






;; Fonts
;; http://ianbits.googlecode.com/svn/trunk/vim/blackboard.vim
(defun sweyla ()
  "Theme generated by Sweyla: http://themes.sweyla.com/seed/682966/"
  (interactive)
  (color-theme-install
   '(sweyla
     ((background-color . "#000207")
      (foreground-color . "#EEEEEE")
      (background-mode . dark)
      (border-color . "#323232")
      (cursor-color . "#00bfff")
      (mouse-color . "#FFFFFF"))
     (mode-line ((t (:foreground "#FFFFFF" :background "#323232"))))
     (region ((t (:background "#323232"))))

     (font-lock-comment-face ((t (:foreground "#aeaeae", :slant italic)))) ; grey
     (font-lock-constant-face ((t (:foreground "#00ff00")))) ; ??
     (font-lock-builtin-face ((t (:foreground "#05AC92")))) ; types, teal
     (font-lock-function-name-face ((t (:foreground "#fa6513")))) ; functions, orange
     (font-lock-variable-name-face ((t (:foreground "#eedd82")))) ; operators, yellow
     (font-lock-keyword-face ((t (:foreground "#f8de33")))) ; yellow
     (font-lock-string-face ((t (:foreground "#64ce3e")))) ;green
     (font-lock-doc-string-face ((t (:foreground "#F2B3A3")))) ; g?
     (font-lock-type-face ((t (:foreground "#8470ff")))) ;; purple class name python

     (flymake-errline ((t (:background "#440044"))))
     (flymake-warnline ((t (:background "#550000"))))
     (mode-line ((t (:foreground "black" :background "light slate gray"))))
     (fringe ((t (:background "#232323"))))



     )))


(provide 'sweyla)
(require 'color-theme)
(setq color-theme-is-global t)
(sweyla)

(set-face-font 'default "DejaVu Sans Mono-9")



;; highlight the current line; set a custom face, so we can
;; recognize from the normal marking (selection)
(defface hl-line '((t (:background "#060622")))
  "Face to use for `hl-line-face'." :group 'hl-line)
(setq hl-line-face 'hl-line)
(global-hl-line-mode t) ; turn it on for all modes by default



(autoload 'markdown-mode "/usr/share/emacs/site-lisp/markdown-mode/markdown-mode.el"
   "Major mode for editing Markdown files" t)
(setq auto-mode-alist
   (cons '("\.md" . markdown-mode) auto-mode-alist))

(defun my-indent-region (N)
  (interactive "p")
  (if mark-active
      (progn (indent-rigidly (min (mark) (point)) (max (mark) (point)) (* N 4))
             (setq deactivate-mark nil))
    (self-insert-command N)))

(defun my-unindent-region (N)
  (interactive "p")
  (if mark-active
      (progn (indent-rigidly (min (mark) (point)) (max (mark) (point)) (* N -4))
             (setq deactivate-mark nil))
    (self-insert-command N)))

(global-set-key (kbd "C-c >") 'my-indent-region)
(global-set-key (kbd "C-c <") 'my-unindent-region)

(setq backup-directory-alist
          `((".*" . ,temporary-file-directory)))
    (setq auto-save-file-name-transforms
          `((".*" ,temporary-file-directory t)))


(defvar current-time-format "%Y-%m-%dT%H:%M:%SZ"
  "Format of date")

(defun insert-timestamp ()
  ; http://stackoverflow.com/questions/251908/how-can-i-insert-current-date-and-time-into-a-file-using-emacs
  "insert the current time (1-week scope) into the current buffer."
       (interactive)
       (insert (format-time-string current-time-format (current-time)))
       )

;; scala
(add-to-list 'load-path "/usr/share/emacs/scala-mode")
(require 'scala-mode-auto)

;(add-to-list 'load-path "/usr/share/ensime/elisp")
;;(add-to-list 'exec-path "/usr/share/ensime")
;(require 'ensime)
;(add-hook 'scala-mode-hook 'ensime-scala-mode-hook)


;; thrift https://gist.github.com/bakkdoor/770490
(require 'font-lock)

(defvar thrift-mode-hook nil)
(add-to-list 'auto-mode-alist '("\\.thrift\\'" . thrift-mode))

(defvar thrift-indent-level 2
  "Defines 2 spaces for thrift indentation.")

;; syntax coloring
(defconst thrift-font-lock-keywords
  (list
   '("#.*$" . font-lock-comment-face)  ;; perl style comments
   '("\\<\\(include\\|struct\\|union\\|namespace\\|exception\\|typedef\\|php_namespace\\|const\\|enum\\|service\\|extends\\|void\\|async\\|throws\\|optional\\|required\\)\\>" . font-lock-keyword-face)  ;; keywords
   '("\\<\\(bool\\|byte\\|i16\\|i32\\|i64\\|double\\|string\\|binary\\|map\\|list\\|set\\)\\>" . font-lock-type-face)  ;; built-in types
   '("\\<\\([A-Z]\\w*\\)\\>" . font-lock-type-face)   ;; typenames (unions & structs)
   '("\\<\\([0-9]+\\)\\>" . font-lock-variable-name-face)   ;; ordinals
   '("\\<\\(\\w+\\)\\s-*(" (1 font-lock-function-name-face))  ;; functions
   )
  "Thrift Keywords")

;; indentation
(defun thrift-indent-line ()
  "Indent current line as Thrift code."
  (interactive)
  (beginning-of-line)
  (if (bobp)
      (indent-line-to 0)
    (let ((not-indented t) cur-indent)
      (if (looking-at "^[ \t]*\\(}\\|throws\\)")
          (if (looking-at "^[ \t]*}")
              (progn
                (save-excursion
                  (forward-line -1)
                  (setq cur-indent (- (current-indentation) thrift-indent-level)))
                (if (< cur-indent 0)
                    (setq cur-indent 0)))
            (progn
              (save-excursion
                (forward-line -1)
                (if (looking-at "^[ \t]*[\\.<>[:word:]]+[ \t]+[\\.<>[:word:]]+[ \t]*(")
                    (setq cur-indent (+ (current-indentation) thrift-indent-level))
                  (setq cur-indent (current-indentation))))))
        (save-excursion
          (while not-indented
            (forward-line -1)
            (if (looking-at "^[ \t]*}")
                (progn
                  (setq cur-indent (current-indentation))
                  (setq not-indented nil))
              (if (looking-at "^.*{[^}]*$")
                  (progn
                    (setq cur-indent (+ (current-indentation) thrift-indent-level))
                    (setq not-indented nil))
                (if (bobp)
                    (setq not-indented nil)))
              (if (looking-at "^[ \t]*throws")
                  (progn
                    (setq cur-indent (- (current-indentation) thrift-indent-level))
                    (if (< cur-indent 0)
                        (setq cur-indent 0))
                    (setq not-indented nil))
                (if (bobp)
                    (setq not-indented nil)))
              (if (looking-at "^[ \t]*[\\.<>[:word:]]+[ \t]+[\\.<>[:word:]]+[ \t]*([^)]*$")
                  (progn
                    (setq cur-indent (+ (current-indentation) thrift-indent-level))
                    (setq not-indented nil))
                (if (bobp)
                    (setq not-indented nil)))
              (if (looking-at "^[ \t]*\\/\\*")
                  (progn
                    (setq cur-indent (+ (current-indentation) 1))
                    (setq not-indented nil))
                (if (bobp)
                    (setq not-indented nil)))
              (if (looking-at "^[ \t]*\\*\\/")
                  (progn
                    (setq cur-indent (- (current-indentation) 1))
                    (setq not-indented nil))
                (if (bobp)
                    (setq not-indented nil)))
              ))))
      (if cur-indent
          (indent-line-to cur-indent)
        (indent-line-to 0)))))

;; C/C++ comments; also allowing underscore in words
(defvar thrift-mode-syntax-table
  (let ((thrift-mode-syntax-table (make-syntax-table)))
    (modify-syntax-entry ?_ "w" thrift-mode-syntax-table)
    (modify-syntax-entry ?/ ". 124b" thrift-mode-syntax-table)
    (modify-syntax-entry ?* ". 23" thrift-mode-syntax-table)
    (modify-syntax-entry ?\n "> b" thrift-mode-syntax-table)
    thrift-mode-syntax-table)
  "Syntax table for thrift-mode")

(defun thrift-mode ()
  "Mode for editing Thrift files"
  (interactive)
  (kill-all-local-variables)
  (set-syntax-table thrift-mode-syntax-table)
  (set (make-local-variable 'font-lock-defaults) '(thrift-font-lock-keywords))
  (setq major-mode 'thrift-mode)
  (setq mode-name "Thrift")
  (run-hooks 'thrift-mode-hook)
  (set (make-local-variable 'indent-line-function) 'thrift-indent-line)
  )
(provide 'thrift-mode)




;; Main

;(server-start)
(setq server-use-tcp t)
(setq server-host "localhost")

;; mobile org
(setq org-directory "~/org/")
(setq org-agenda-files (quote ("~/org/")))
(setq org-mobile-inbox-for-pull "~/org/index.org")
(setq org-mobile-directory "~/Dropbox/org")




;; ruby ends stuff
;;(add-to-list 'load-path "~/dotfiles/elisp/ruby-end")
;;(require 'ruby-end)
