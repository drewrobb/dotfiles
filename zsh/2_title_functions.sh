# http://www.davidpashley.com/articles/xterm-titles-with-bash.html
case $TERM in
    *rxvt|*term)

        # set window title to hostname:pwd
        precmd_title() { print -Pn "\e]0;%m:%~\a" }
        precmd_functions+='precmd_title'

        # set title to pwd?
        preexec_title () { print -Pn "\e]0;$1\a" }
        preexec_functions+='preexec_title'

        ;;
esac
