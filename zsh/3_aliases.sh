alias kgs='aoss javaws http://files.gokgs.com/javaBin/cgoban.jnlp'
alias von='sudo vbetool dpms on'
alias voff='sudo vbetool dpms off'
alias grep='grep --color -I --exclude="*\.git/*"'
alias gedit='emacs'
alias nano='emacs'
alias e='emacs -nw'
alias se='sudo emacs -nw'
alias cd='pushd'
alias pd='popd'
alias 'git diff'='git diff --color'
alias ts='python -c "import time; print int(time.time())"'

# default editors by firetype with alias -s
alias -s pdf=evince
alias -s png=feh
alias -s jpg=feh
alias -s jpeg=feh
alias -s gif=feh

#alias cd='cd && ls --color'
alias rtfm='man'

export OLDPWD=/home/drew

keep () {
    kept=()     # Erase old value in case of error on next line
    kept=($~*)
    print -Rc - $kept
}
alias keep='noglob keep '
alias myip='curl whatismyip.org/'