font_prefix="xft:inconsolata:medium:antialias=true:hinting:false:pixelsize="

font_size=13

increase_font() {
    font_size=$(($font_size+1))
    set_font
}

decrease_font() {
    font_size=$(($font_size-1))
    set_font
}


set_font() {
    printf '\33]50;%s\007' $font_prefix$font_size
}

zle -N increase_font
zle -N decrease_font

bindkey -e '^$terminfo[ACS_PLUS]' increase_font
bindkey '^k' increase_font
bindkey '^j' decrease_font
