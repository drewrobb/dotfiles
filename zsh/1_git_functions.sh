
##### Git branch in prompt

export __CURRENT_GIT_BRANCH=
export __CURRENT_GIT_VARS_INVALID=1

zsh_git_invalidate_vars() {
    export __CURRENT_GIT_VARS_INVALID=1
}

zsh_git_compute_vars() {
    export __CURRENT_GIT_BRANCH="$(parse_git_branch)"
    export __CURRENT_GIT_VARS_INVALID=
}

parse_git_branch() {
    git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}


zsh_git_chpwd_update_vars() {
    zsh_git_invalidate_vars
}
chpwd_functions+='zsh_git_chpwd_update_vars'


zsh_git_preexec_update_vars() {
    case "$(history $HISTCMD)" in
        *git*) zsh_git_invalidate_vars;;
    esac
}
preexec_functions+='zsh_git_preexec_update_vars'

function zsh_git_prompt_precmd {
    exit=$?

    if [ $exit_code -n 0 ]
        then
        true
    fi


    if [[ -n "$__CURRENT_GIT_VARS_INVALID" ]] ; then
        zsh_git_compute_vars
        # re generate prompt with new git info
        rprompt
    fi
}
precmd_functions+='zsh_git_prompt_precmd'


get_git_prompt_info() {
    test -n "$__CURRENT_GIT_VARS_INVALID" && zsh_git_compute_vars
}