# next lets set some enviromental/shell pref stuff up
TERM='xterm'
zmodload zsh/terminfo
# setopt NOHUP
#setopt NOTIFY
#setopt NO_FLOW_CONTROL
setopt INC_APPEND_HISTORY SHARE_HISTORY
setopt APPEND_HISTORY
# setopt AUTO_LIST      # these two should be turned off
# setopt AUTO_REMOVE_SLASH
# setopt AUTO_RESUME        # tries to resume command of same name
unsetopt BG_NICE        # do NOT nice bg commands
#unsetopt CORRECT           # command CORRECTION
setopt EXTENDED_HISTORY     # puts timestamps in the history
# setopt HASH_CMDS      # turns on hashing
#
setopt MENUCOMPLETE
setopt ALL_EXPORT

# Set/unset  shell options
setopt   notify globdots correct pushdtohome cdablevars autolist

# annoyingly tries to parse arguments as filenames
#setopt   correctall

setopt   autocd recexact longlistjobs
setopt   autoresume histignoredups pushdsilent
setopt   autopushd pushdminus extendedglob rcquotes mailwarning
unsetopt bgnice autoparamslash

# no annoying beep
unsetopt HIST_BEEP

#setopt rm_star_wait
# Autoload zsh modules when they are referenced
zmodload -a zsh/stat stat
zmodload -a zsh/zpty zpty
zmodload -a zsh/zprof zprof

### clear vars
chpwd_functions=
preexec_functions=
precmd_functions=

# setup hook function lists
typeset -ga preexec_functions
typeset -ga precmd_functions
typeset -ga chpwd_functions

for zshrc_snipplet in /home/$USER/dotfiles/zsh/* ; do
    source $zshrc_snipplet
done

HISTFILE=$HOME/.zhistory
HISTSIZE=10000000
SAVEHIST=10000000
HOSTNAME="`hostname`"
PAGER='less'
EDITOR='emacsclient --alternate-editor emacs'

autoload -U colors
autoload zsh/terminfo
colors


for color in RED GREEN YELLOW WHITE BLUE; do
    eval PR_$color='%{$fg[${(L)color}]%}'
    eval PR_BRIGHT_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
done

PR_NO_COLOR="%{$terminfo[sgr0]%}"


HOST_PATH='/etc/internal_hostname'
if [ -f $HOST_PATH ]
then
    HOSTNAME=`cat $HOST_PATH`
fi

HOSTCOLOR=$PR_BRIGHT_RED
if [ $HOSTNAME = "stinkpad" ]
then
    HOSTCOLOR=$PR_BRIGHT_GREEN
fi

if [ $HOSTNAME = "tera" ]
then
    HOSTCOLOR=$PR_BRIGHT_GREEN
fi


# display $ or red $ depending on exit code
RETURNCODE="%(?.$.%{$fg[red]%}$%{$PR_NO_COLOR%})"

lprompt() {
    PS1="[$PR_BRIGHT_BLUE%n%{$fg[white]%}@$HOSTCOLOR%U%B$HOSTNAME%u%b$PR_NO_COLOR:%B%{$fg[red]%}%2c$PR_NO_COLOR]%b$RETURNCODE "
}
lprompt

rprompt() {
    RPS1="$__CURRENT_GIT_BRANCH %{$fg[yellow]%}%D{%H:%M:%S}$PR_NO_COLOR"
}
rprompt


unsetopt ALL_EXPORT


update(){
        echo "Updating zshrc from $URL..."
        echo "Press Ctrl+C within 5 seconds to abort..."
        cd ~/dotfiles && git pull origin master && source ~/.zshrc && pd
}


ex  () {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xjf $1        ;;
            *.tar.gz)    tar xzf $1     ;;
            *.bz2)       bunzip2 $1       ;;
            *.rar)       unrar e $1     ;;
            *.gz)        gunzip $1     ;;
            *.tar)       tar xf $1        ;;
            *.tbz2)      tar xjf $1      ;;
            *.tgz)       tar xzf $1       ;;
            *.zip)       unzip $1     ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1    ;;
            *)           echo "'$1' cannot be extracted via extract()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}


autoload -U compinit
compinit
bindkey "^?" backward-delete-char
bindkey '^[OH' beginning-of-line
bindkey '^[OF' end-of-line
bindkey '^[[5~' up-line-or-history
bindkey '^[[6~' down-line-or-history
bindkey "^r" history-incremental-search-backward
bindkey ' ' magic-space    # also do history expansion on space
bindkey '^I' complete-word # complete on tab, leave expansion to _expand
zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path ~/.zsh/cache/$HOST

zstyle ':completion:*' list-colors /usr/share/$LS_COLORS
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' menu select=1 _complete _ignored _approximate
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'

# Completion Styles

# list of completers to use
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate

# allow one error for every three characters typed in approximate completer
zstyle -e ':completion:*:approximate:*' max-errors \
    'reply=( $(( ($#PREFIX+$#SUFFIX)/2 )) numeric )'

# insert all expansions for expand completer
zstyle ':completion:*:expand:*' tag-order all-expansions

# formatting and messages
zstyle ':completion:*' verbose yes
zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
zstyle ':completion:*' group-name ''

# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# offer indexes before parameters in subscripts
zstyle ':completion:*:*:-subscript-:*' tag-order indexes parameters

# command for process lists, the local web server details and host completion
# on processes completion complete all user processes
# zstyle ':completion:*:processes' command 'ps -au$USER'

## add colors to processes for kill completion
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'

#zstyle ':completion:*:processes' command 'ps ax -o pid,s,nice,stime,args | sed "/ps/d"'
zstyle ':completion:*:*:kill:*:processes' command 'ps --forest -A -o pid,user,cmd'
zstyle ':completion:*:processes-names' command 'ps axho command'
#zstyle ':completion:*:urls' local 'www' '/var/www/htdocs' 'public_html'
#
#NEW completion:
# 1. All /etc/hosts hostnames are in autocomplete
# 2. If you have a comment in /etc/hosts like #%foobar.domain,
#    then foobar.domain will show up in autocomplete!
zstyle ':completion:*' hosts $(awk '/^[^#]/ {print $2 $3" "$4" "$5}' /etc/hosts | grep -v ip6- && grep "^#%" /etc/hosts | awk -F% '{print $2}')
# Filename suffixes to ignore during completion (except after rm command)
zstyle ':completion:*:*:(^rm):*:*files' ignored-patterns '*?.o' '*?.c~' \
    '*?.old' '*?.pro'
# the same for old style completion
#fignore=(.o .c~ .old .pro)

# SSH Completion
zstyle ':completion:*:scp:*' tag-order \
   files users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:scp:*' group-order \
   files all-files users hosts-domain hosts-host hosts-ipaddr
zstyle ':completion:*:ssh:*' tag-order \
   users 'hosts:-host hosts:-domain:domain hosts:-ipaddr"IP\ Address *'
zstyle ':completion:*:ssh:*' group-order \
   hosts-domain hosts-host users hosts-ipaddr
zstyle '*' single-ignored show


# # --------------------------------------------------------------------
# # aliases
# # --------------------------------------------------------------------

alias df='df -h'
alias man='LC_ALL=C LANG=C man'
alias l='ls'
alias ll='ls -al'
alias ls='ls --color=auto '
alias myip='curl ifconfig.me'
alias grep="grep --color=always --exclude-dir='.git' --exclude='*min.js' --exclude='*min.css'"
alias xlock="i3lock -c 000000"
alias ec='emacsclient -n'

##### Setup distro customized package manager shortcuts

# ArchLinux
if [ -f /etc/arch-release ]
then
    aur_wrapper='yaourt'
    pkg_search='$aur_wrapper -Ss'
    pkg_install='$aur_wrapper -Sy'
    pkg_remove='$aur_wrapper -R'
    pkg_update='$aur_wrapper -Syu'

# Debian
elif [ -f /etc/debian_version ]
then
    pkg_search='apt-cache search'
    pkg_install='sudo apt-get install'
    pkg_remove='sudo apt-get remove'
    pkg_update='sudo apt-get update'
fi

alias ai=$pkg_install
alias as=$pkg_search
alias ar=$pkg_remove
alias au=$pkg_update



REPORTTIME=1                # report time if execution exceeds amount of seconds



#export PERL_MM_USE_DEFAULT=1
#eval $(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)
eval $( dircolors -b /usr/share/LS_COLORS )


PYTHONDONTWRITEBYTECODE=T
export PYMACS_PYTHON=python2


fpath=($HOME/dotfiles $fpath)


eval `keychain --eval --nogui -Q -q id_dsa`

CUDADIR=/opt/cuda-toolkit
CUDA_HOME=/opt/cuda-toolkit
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/cuda-toolkit/lib:/opt/cuda-toolkit/lib64
PATH=$PATH:/opt/cuda-toolkit/bin

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

# rails startup optimization?
# http://stefanwienert.net/blog/2012/07/13/how-to-accelerate-rails-3-starting-up-time-under-ruby-1-dot-9-3/
export RUBY_HEAP_MIN_SLOTS=800000
export RUBY_HEAP_FREE_MIN=100000
export RUBY_HEAP_SLOTS_INCREMENT=300000
export RUBY_HEAP_SLOTS_GROWTH_FACTOR=1
export RUBY_GC_MALLOC_LIMIT=79000000
