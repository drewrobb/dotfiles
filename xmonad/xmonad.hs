
-- Import stuff
import XMonad
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import XMonad.Util.EZConfig(additionalKeys)
import System.Exit
import Graphics.X11.Xlib
import System.IO


-- actions
import XMonad.Actions.CycleWS
import XMonad.Actions.WindowGo
import qualified XMonad.Actions.Search as S
import XMonad.Actions.Search
import qualified XMonad.Actions.Submap as SM
import XMonad.Actions.GridSelect

import qualified XMonad.Actions.FlexibleManipulate as Flex


-- utils
import XMonad.Util.Scratchpad (scratchpadSpawnAction, scratchpadManageHook, scratchpadFilterOutWorkspace)
import XMonad.Util.Run(spawnPipe)
import qualified XMonad.Prompt          as P
import XMonad.Prompt.Shell
import XMonad.Prompt


-- hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.UrgencyHook
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetCursor

-- layouts
import XMonad.Layout.NoBorders
import XMonad.Layout.MouseResizableTile
import XMonad.Layout.Reflect
import XMonad.Layout.IM
import XMonad.Layout.PerWorkspace (onWorkspace)
import XMonad.Layout.Grid
import XMonad.Layout.TabbedNoKill
import XMonad.Layout.Accordion
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile
import qualified XMonad.Layout as L

-- patch to not have tiled layer focus jump when using floating layer
-- http://code.google.com/p/xmonad/issues/detail?id=4
import XMonad.Layout.TrackFloating


-- allow resize floating windows from any corner
import qualified XMonad.Actions.FlexibleResize as Flex

-- Data.Ratio for IM layout
import Data.Ratio ((%))


import XMonad.Util.EZConfig


import qualified XMonad.Actions.ConstrainedResize as Sqr

import XMonad.Layout.WindowNavigation
import XMonad.Layout.Spiral
import Data.Ratio


import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.CopyWindow(copy)

-- contains 'unless'
import Control.Monad


import XMonad.Util.Themes


import XMonad.Layout.Renamed

-- Notes:
-- avoidStructs - give space for xmobar

myFont = "xft:inconsolata:pixelsize=13:antialias=true:hinting=true"



-- Main --
main = do
  hRight <- spawnPipe "xmobar -x 1"
  xmonad $ withUrgencyHook NoUrgencyHook $ ewmh defaultConfig
                       { startupHook        = myStartupHook
                       , manageHook         = myManageHook
                       , layoutHook         = avoidStruts $ myL
                       , borderWidth        = myBorderWidth
                       , normalBorderColor  = myNormalBorderColor
                       , focusedBorderColor = myFocusedBorderColor
                       , keys               = myKeys
                       --, mouseBindings      = myMouseBindings
                       , logHook            = do
                           myLogHook hRight
                       , modMask            = myModMask
                       , terminal           = myTerminal
                       , workspaces         = myWorkspaces
                       , focusFollowsMouse  = True
                       }


myStartupHook :: X ()
myStartupHook = do
  setDefaultCursor 68


--myMouse x  = [ (modMask, button4), (\w -> focus w >> Flex.mouseResizeWindow w)]
--newMouse x = M.union (mouseBindings defaultConfig x) (M.fromList (myMouse x))
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
 [

   ((modm, button1), (\w -> focus w >> Flex.mouseWindow Flex.position w)) -- left button
 , ((modm, button3), (\w -> focus w >> Flex.mouseWindow Flex.resize w)) -- right button
 , ((modm, button4), (\_ -> windows W.focusUp))
 --, ((modm, button4), (\_ -> windows W.focusUp))
 --, ((modm, button5), (\_ -> windows W.focusDown))
 , ((modm .|. shiftMask, button2), (\w -> focus w >> Sqr.mouseResizeWindow w True >> windows W.shiftMaster))
 , ((modm .|. shiftMask, button4), (\_ -> windows W.swapUp))
 , ((modm .|. shiftMask, button5), (\_ -> windows W.swapDown))
 , ((modm .|. controlMask, button4), (\_ -> prevWS))
 , ((modm .|. controlMask, button5), (\_ -> nextWS))
        ]


-- hooks
-- automaticly switching app to workspace
myManageHook :: ManageHook
myManageHook = scratchpadManageHook (W.RationalRect 0.25 0.375 0.5 0.35) <+> ( composeAll . concat $
                [[isFullscreen                      --> doFullFloat
                , className =? "OpenOffice.org 3.1" --> doShift "5:doc"
                , className =?  "Xmessage"          --> doCenterFloat
                , className =?  "MPlayer"           --> doFullFloat
                , title     =? "Figure 1"           --> doCenterFloat
                , title     =? "Flow"               --> doCenterFloat
                , className =? "feh"                --> doCenterFloat
                , className =? "Gimp"               --> doShift "9:gimp"
                , className =? "uzbl"               --> doShift "2:web"
                , className =? "vimprobable"        --> doShift "2:web"
                , className =? "Pidgin"             --> doShift "1:chat"
                , className =? "Skype"              --> doShift "1:chat"
                , className =? "MPlayer"            --> doShift "8:vid"
                , className =? "VirtualBox"         --> doShift "6:virtual"
                , className =? "Apvlv"              --> doShift "4:pdf"
                , className =? "Evince"             --> doShift "4:pdf"
                , className =? "Epdfview"           --> doShift "4:pdf"
                , className =? "Remmina"            --> doShift "6:vbox"]
                ]
                                                                             )  <+> manageDocks

customPP :: PP
customPP = defaultPP {
             ppCurrent         = xmobarColor "#00FF00" "".wrap "[" "]",
             ppVisible         = xmobarColor "#FFFF00" "".wrap "<" ">",
             ppHidden          = xmobarColor "#FFFFFF" "",
             ppHiddenNoWindows = xmobarColor "#999999" "",
             ppTitle           = xmobarColor "#FFFFFF" "".shorten 80,
             ppSep             = "<fc=#FFFFFF> | </fc>",
             ppUrgent          = xmobarColor "#FF0000" "".wrap "!" "!"
           }

--logHook
myLogHook :: Handle -> X ()
myLogHook h = dynamicLogWithPP $ customPP { ppOutput = hPutStrLn h }



-- some nice colors for the prompt windows to match the dzen status bar.
myXPConfig = defaultXPConfig
    {
       font  = myFont
        ,fgColor = "#00FFFF"
        , bgColor = "#000000"
        , bgHLight    = "#000000"
        , fgHLight    = "#FF0000"
        , position = Top
    }



myTheme = defaultTheme { activeColor         = "#444444"
                       , inactiveColor       = "#000000"
                       , activeBorderColor   = "#BBBBBB"
                       , inactiveBorderColor = "#999999"
                       , activeTextColor     = "white"
                       , inactiveTextColor   = "grey"
                       , decoHeight          = 16
                       , fontName            = myFont
                     }


-- windowSwitcherDecoration shrinkText (theme smallClean) (draggingVisualizer $
myL = avoidStruts $ ( trackFloating tabb ||| ResizableTall 1 (3/100) (2/3) [] ||| Mirror (ResizableTall 1 (3/100) (2/3) []))
   where
     --foo  = mouseResizableTile{draggerType = FixedDragger{ gapWidth = 2, draggerWidth = 2}}
     --grid   = renamed [Replace "grid"] Grid
     tabb   = noBorders $ tabbed shrinkText myTheme



myTerminal = "urxvt --loginShell"
myModMask = mod4Mask



-- borders
myBorderWidth :: Dimension
myBorderWidth = 1
--
myNormalBorderColor, myFocusedBorderColor :: String
myNormalBorderColor = "#007700"
myFocusedBorderColor = "#00FF00"
--


--Workspaces
myWorkspaces :: [WorkspaceId]
myWorkspaces = ["1", "2", "3", "4", "5", "6" ,"7", "8", "9"]
--

-- Switch to the "web" workspace
viewWeb = windows (W.greedyView "2:web")                           -- (0,0a)
--

--Search engines to be selected :  [google (g), wikipedia (w) , youtube (y) , maps (m), dictionary (d) , wikipedia (w), bbs (b) ,aur (r), wiki (a) , TPB (t), mininova (n), isohunt (i) ]
--keybinding: hit mod + s + <searchengine>
searchEngineMap method = M.fromList $
       [ ((0, xK_g), method S.google )
       , ((0, xK_y), method S.youtube )
       , ((0, xK_m), method S.maps )
       , ((0, xK_d), method S.dictionary )
       , ((0, xK_w), method S.wikipedia )
       , ((0, xK_h), method S.hoogle )
       , ((0, xK_i), method S.isohunt )
       , ((0, xK_b), method $ S.searchEngine "archbbs" "http://bbs.archlinux.org/search.php?action=search&keywords=")
       , ((0, xK_r), method $ S.searchEngine "AUR" "http://aur.archlinux.org/packages.php?O=0&L=0&C=0&K=")
       , ((0, xK_a), method $ S.searchEngine "archwiki" "http://wiki.archlinux.org/index.php/Special:Search?search=")
       ]


myRestart = "for pid in `pgrep dzen2`; do kill -9 $pid; done && xmonad --recompile && xmonad --restart"






shellXPC :: XPConfig
shellXPC = defaultXPConfig { bgColor  = "black"
                       , fgColor  = "grey"
                       , promptBorderWidth = 0
                       , position = Bottom
                       , height   = 16
                       , historySize = 256
                       , font = myFont }


myKeys c = mkKeymap c $                                 -- keys; uses EZConfig
     [ ("M-<Return>",  spawn $ XMonad.terminal c)       -- spawn terminal
     , ("M-r"         ,  shellPrompt shellXPC)                 -- spawn menu program, uses Shell
     , ("M-s"         ,  search)                          -- search websites, uses Search & Submap
     , ("M-S-c"       ,  kill)                            -- kill window
     , ("M-<Space>"   ,  sendMessage NextLayout)          -- next layout
     , ("M-S-<Space>" ,  setLayout $ XMonad.layoutHook c) -- default layout
     , ("M-n"         ,  refresh)                         -- resize to correct size
     , ("M-j"         ,  windows W.focusDown)             -- move focus; next window
     , ("M-k"         ,  windows W.focusUp)               -- move focus; prev. window
     , ("M-m"         ,  windows W.focusMaster)           -- focus on master
     --, ("M-<Return>"  ,  windows W.swapMaster)            -- swap current with master
     , ("M-S-j"       ,  windows W.swapDown)              -- swap focused with next window
     , ("M-S-k"       ,  windows W.swapUp)                -- swap focused with prev. window
     , ("M-h"         ,  sendMessage Shrink)              -- shrink master area
     , ("M-l"         ,  sendMessage Expand)              -- expand master area
     , ("M-t"         ,  withFocused $ windows . W.sink)  -- put window back on tiling layer
     , ("M-,"         ,  sendMessage (IncMasterN 1))      -- increase number of windows in master pane
     , ("M-."         ,  sendMessage (IncMasterN (-1)))   -- decrease number of windows in master pane
     , ("M-b"         ,  sendMessage ToggleStruts)        -- toggle status bar gap, uses ManageDocks

     , ("C-S-q"       ,  io (exitWith ExitSuccess))       -- exit xmonad


    , ("M-x <Right>",                sendMessage $ Go R)
    , ("M-x <Left",                  sendMessage $ Go L)
    , ("M-x <Up>",                  sendMessage $ Go U)
    , ("M-x <Down>",                sendMessage $ Go D)

    , ("M-q", spawn "i3lock -c 000000") -- lock screen


    , ("M-g", goToSelected defaultGSConfig)


    , ("M-u", focusUrgent) -- focuse most recently urgent window



    -- volume control
    , ("M-<Up>"      , spawn "amixer -q set Master 2dB+")
    , ("M-<Down>"    , spawn "amixer -q set Master 2dB-")
    , ("M-<Pause>"   , spawn "amixer -q set Master toggle")



    -- dynamic workspaces
   , ("M-S-<Backspace>", removeWorkspace)
   , ("M-S-v", selectWorkspace defaultXPConfig)
   --, ((modm, xK_m                    ), withWorkspace defaultXPConfig (windows . W.shift))
   , ("M-S-m", withWorkspace defaultXPConfig (windows . copy))
   --, ((modm .|. shiftMask, xK_r      ), renameWorkspace defaultXPConfig)




    --, ("M-x <Down>",                spawn "xmessage foo")

     ] ++
     -- mod-[1..9], Switch to workspace N
     -- mod-shift-[1..9], Move client to workspace N



    --zip (zip (repeat (modMask)) [xK_1..xK_9]) (map (withNthWorkspace W.greedyView) [0..])
    -- ++
    --zip (zip (repeat (modMask .|. shiftMask)) [xK_1..xK_9]) (map (withNthWorkspace W.shift) [0..])


     [(m ++ k, windows $ f w)
         | (w, k) <- zip (XMonad.workspaces c) (map show [1..9])
         , (m, f) <- [("M-",W.greedyView), ("M-S-",W.shift)]]

    where searchSite = S.promptSearchBrowser shellXPC "ff3"
          search     = SM.submap . mkKeymap c $
                      [("g", searchSite S.google)
                      ,("h", searchSite S.hoogle)
                      ,("a", searchSite S.amazon)
                      ,("i", searchSite S.imdb)
                      ,("y", searchSite S.youtube)]